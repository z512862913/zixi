# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Student',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('student_id', models.CharField(max_length=13, verbose_name='\u5b66\u53f7')),
                ('zixi_time_total', models.PositiveIntegerField(verbose_name='\u603b\u7684\u81ea\u4e60\u65f6\u95f4')),
            ],
        ),
    ]
