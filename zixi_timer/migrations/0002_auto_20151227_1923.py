# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('zixi_timer', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='student',
            name='zixi_time_total',
            field=models.PositiveIntegerField(default=0, verbose_name='\u603b\u7684\u81ea\u4e60\u65f6\u95f4'),
        ),
    ]
