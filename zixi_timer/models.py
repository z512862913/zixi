# -*- coding: utf-8 -*-
from django.db import models


# Create your models here.
class Student(models.Model):
    student_id = models.CharField(u'学号', max_length=13)
    zixi_time_total = models.PositiveIntegerField(u'总的自习时间', default=0)