# -*- coding: utf-8 -*-
import time

from django.http import JsonResponse

from zixi_timer.models import *


# 取出用户ip
def get_ip(request):
    if request.META.has_key('HTTP_X_FORWARDED_FOR'):
        ip =  request.META['HTTP_X_FORWARDED_FOR']
    else:
        ip = request.META['REMOTE_ADDR']
    return ip


# 记录rcv_time函数日志
def logging_rcv_time(ip, request_method, message, student_id='XXXXXXXXXXXXX', zixi_time=-666, success=False):
    with open('HtmlRequest_rcv_time.log','a') as f:
        today = time.strftime('%Y-%m-%d-%H-%M-%S', time.localtime(time.time()))
        f.write(today+'\t')
        f.write(str(ip)+'\t')
        f.write(str(request_method)+'\t')
        f.write(str(message)+'\t')
        f.write(str(student_id)+'\t')
        f.write(str(zixi_time)+'\t')
        f.write(str(success)+'\t')
        f.write('\n')


# 记录get_total_time函数日志
def logging_get_total_time(ip, request_method, message, student_id='XXXXXXXXXXXXX', totoal_zixi_time=0, success=False):
    with open('HtmlRequest_get_total_time.log','a') as f:
        today = time.strftime('%Y-%m-%d-%H-%M-%S', time.localtime(time.time()))
        f.write(today+'\t')
        f.write(str(ip)+'\t')
        f.write(str(request_method)+'\t')
        f.write(str(message)+'\t')
        f.write(str(student_id)+'\t')
        f.write(str(totoal_zixi_time)+'\t')
        f.write(str(success)+'\t')
        f.write('\n')


# 接收POST方法发出的学号和本次自习时间，单位为分钟
# 存入数据库的是学号对应的总的自习时间
# 数据只记录早上六点开始到第二天早上两点之间的数据
def rcv_time(request):
    ip = get_ip(request)
    if request.method == 'POST':
        hour = time.localtime(time.time()).tm_hour
        student_id = request.POST.get('student_id')
        zixi_time_str = request.POST.get('zixi_time')
        if 2 <= hour < 6:
            logging_rcv_time(ip, request.method, 'time for sleep', student_id)
            result = {'success': False, 'message': 'it’s time for sleep'}
            return JsonResponse(data=result, status=400)
        else:
            if student_id:
                if zixi_time_str:
                    try:
                        zixi_time_int = int(zixi_time_str)
                    except ValueError:
                        logging_rcv_time(ip, request.method, 'not a integer', student_id)
                        result = {'success': False, 'message': 'zixi_time must be a integer'}
                        return JsonResponse(result, status=400)
                    if zixi_time_int >=0:
                        try:
                            student = Student.objects.get(student_id=student_id)
                        except Student.DoesNotExist:
                            student = Student(student_id=student_id)
                        student.zixi_time_total += zixi_time_int
                        student.save()
                        logging_rcv_time(ip, request.method, 'post success', student_id, zixi_time_int, success=True)
                        result = {'success': True, 'message': 'post success'}
                        return JsonResponse(result, status=200)
                    else:
                        logging_rcv_time(ip, request.method, 'not positive',
                                student_id, zixi_time_int)
                        result = {'success': False, 'message': 'zixi_time must be a positive integer'}
                        return JsonResponse(result, status=400)
                else:
                    logging_rcv_time(ip, request.method, 'need zixi_time', student_id)
                    result = {'success': False, 'message': 'need zixi_time'}
                    return JsonResponse(result, status=400)
            else:
                logging_rcv_time(ip, request.method, 'need student ID')
                result = {'success': False, 'message': 'need student ID'}
            return JsonResponse(result, status=400)
    else:
        logging_rcv_time(ip, request.method, 'method error')
        result = {'success': False, 'message': 'method %s is not supported' % request.method}
        return JsonResponse(data=result, status=405)


# 接受一个GET方法发出的带有学号的请求
# 取出学号对应的总的自习时间
def get_total_time(request):
    ip = get_ip(request)
    if request.method == 'POST':
        student_id = request.POST.get('student_id')
        if student_id:
            try:
                student = Student.objects.get(student_id=student_id)
            except Student.DoesNotExist:
                logging_get_total_time(ip, request.method, 'no data', student_id)
                result = {'success': True, 'message': 'this student ID do not have any data',
                          'total_time':0, 'student_id':student_id}
                return JsonResponse(result, status=200)
            logging_get_total_time(ip, request.method, 'get success',
                    student_id, student.zixi_time_total, success=True)
            result = {'success': True, 'message': 'get total time success',
                      'total_time':student.zixi_time_total, 'student_id':student_id}
            return JsonResponse(result, status=200)
        else:
            logging_get_total_time(ip, request.method, 'need student ID')
            result = {'success': False, 'message': 'need student ID'}
            return JsonResponse(result, status=400)
    else:
        logging_get_total_time(ip, request.method, 'method error')
        result = {'success': False, 'message': 'method %s is not supported' % request.method}
        return JsonResponse(data=result, status=405)